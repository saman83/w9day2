// const name = 'Saman';
// console.log(name);


// //Normal function
// function greet() {
//     console.log('Hello from greet');
    
// // }

// // function greet2(name) {
// //     //New Excecution context
// //     console.log('Hello ' + name + ' from greet');
    
// // }

// // //Function that returns a value
// // function getFullName(name, surname) {
// //     return name + ' ' + surname;
        
// // }

// // const fullName = getFullName('Saman', 'Sandy');

// //Objects

// console.log(this);


// const person = {
//     name: 'Saman',
//     surname: 'Sandy',
//     email: 'sasoc76_mil@hotmail.com',
//     address: {
//         street: 'Sturehillsvägen 5'
//     },
//     getFullName: function() {
//         console.log(this);
//     }
// // };

// function Person() {
//     this.name = 'Dewald';
//     this.surname = 'Els';
//     this.email = 'sasoc76_mil@hotmail.com';
// }

// const Dewald = new Person();

//Task1
// function frontEnd(num1, num2) {
//     return num1*num2;
    
// }


//Task2
// function frontEnd(num1, num2, num3) {
//     return num1+num2+num3;
    
// }


//Task3
// function frontEnd(num1) {
//     return num1/2;
    
// }

//Task4
// function frontEnd(num1, num2, num3) {
//     return (num1+num2)*num3; 
// }


//How Block scooped variables work
// function f(input) {
//     let a = 100;


//     if (input) {
//         // Still okay to reference 'a'
//         let b = a + 1;
//         // return b because the variable works inside the curly braces
//     }

//     // Error: 'b' doesn't exist here
//     return b;
// }


//How Hoisting works

//This returns 5
// var x=5;

// console.log(x);

//This returns Undefined

// console.log(x);
// var x=5;

//Give example of 5 Coercion using different data types
// console.log(true + false);
// console.log(null-1);
// console.log(0 === false);
// console.log("17" == 17);
// console.log(0 === false);



